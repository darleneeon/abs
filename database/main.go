package database

import (
	"encoding/binary"
	"errors"
	"log"
	"os"
	"os/user"
	"time"

	"github.com/boltdb/bolt"
)

var databaseFile string

const topicsBucket = "topics"

func init() {
	// Generate path to the database file for current user
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}

	databaseFile = usr.HomeDir + string(os.PathSeparator) + ".abs.db"

	// Connect to the database
	db, err := connectDB()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Create 'topics' bucket if doesn't exists
	if err = db.Update(func(tx *bolt.Tx) error {
		_, err = tx.CreateBucketIfNotExists([]byte(topicsBucket))
		return err
	}); err != nil {
		log.Fatal(err)
	}
}

// connectDB returns a database instance
func connectDB() (*bolt.DB, error) {
	db, err := bolt.Open(databaseFile, 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		err = errors.New("Connecting to the database: " + err.Error())
		return nil, err
	}

	return db, nil
}

// itob returns an 8-byte big endian representation of v
func itob(v int) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(v))
	return b
}
