package database

// Abstract model (table)
type Abstract struct {
	Heading string `json:"heading"` // Heading of abstract
	Link    string `json:"link"`    // Link to the text (like telegra.ph, pastebin.com)
}
