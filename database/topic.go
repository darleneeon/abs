package database

import (
	"encoding/json"
	"errors"

	"github.com/boltdb/bolt"
)

// Topic model (table)
type Topic struct {
	ID        int
	Heading   string     `json:"heading"`
	Abstracts []Abstract `json:"abstracts"`
}

// Save saves the topic
func (t *Topic) Save() error {
	db, err := connectDB()
	if err != nil {
		return err
	}
	defer db.Close()

	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(topicsBucket))

		// Generate ID for the topic
		id, _ := b.NextSequence()
		t.ID = int(id)

		// Marshal topic data into bytes
		buf, err := json.Marshal(t)
		if err != nil {
			return err
		}

		return b.Put(itob(t.ID), buf)
	})
}

// Remove removes the topic
func (t *Topic) Remove() error {
	db, err := connectDB()
	if err != nil {
		return err
	}
	defer db.Close()

	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(topicsBucket))

		return b.Delete(itob(t.ID))
	})
}

// AddAbstract adds the abstract
func (t *Topic) AddAbstract(a Abstract) error {
	t.Abstracts = append(t.Abstracts, a)

	db, err := connectDB()
	if err != nil {
		return err
	}
	defer db.Close()

	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(topicsBucket))

		// Marshal topic data into bytes
		buf, err := json.Marshal(t)
		if err != nil {
			return err
		}

		return b.Put(itob(t.ID), buf)
	})
}

// RemoveAbstract removes the abstract
func (t *Topic) RemoveAbstract(id int) error {
	t.Abstracts = append(t.Abstracts[:id], t.Abstracts[id+1:]...)

	db, err := connectDB()
	if err != nil {
		return err
	}
	defer db.Close()

	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(topicsBucket))

		// Marshal topic data into bytes
		buf, err := json.Marshal(t)
		if err != nil {
			return err
		}

		return b.Put(itob(t.ID), buf)
	})
}

// GetTopics returns all topics
func GetTopics() ([]Topic, error) {
	var topics []Topic

	db, err := connectDB()
	if err != nil {
		return []Topic{}, err
	}
	defer db.Close()

	if err = db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(topicsBucket))
		c := b.Cursor()

		// Prepare slice for topics
		topics = make([]Topic, 0, int(b.Sequence()))

		// Fill the slice
		var topic Topic

		for k, v := c.First(); k != nil; k, v = c.Next() {
			topic, err = unmarshalTopic(v)
			if err != nil {
				return err
			}

			topics = append(topics, topic)
		}

		return nil
	}); err != nil {
		return []Topic{}, err
	}

	return topics, nil
}

// GetTopic returns the topic
func GetTopic(id int) (Topic, error) {
	var topic Topic

	db, err := connectDB()
	if err != nil {
		return Topic{}, err
	}
	defer db.Close()

	if err = db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(topicsBucket))

		data := b.Get(itob(id))
		topic, err = unmarshalTopic(data)
		if err != nil {
			return err
		}

		return nil
	}); err != nil {
		return Topic{}, err
	}

	return topic, nil
}

func unmarshalTopic(data []byte) (Topic, error) {
	var topic Topic

	if err := json.Unmarshal(data, &topic); err != nil {
		err = errors.New("Topic not found")
		return Topic{}, err
	}

	return topic, nil
}
