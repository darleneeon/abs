package cmd

import (
	"errors"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/darleneeon/abs/database"
)

// raCmd represents the ra command
var raCmd = &cobra.Command{
	Use:   "ra [TOPIC ID] [ABSTRACT ID]",
	Short: "Remove abstract from the topic",
	Args:  cobra.MinimumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {

		// Get topic id argument
		topicID, err := strconv.Atoi(args[0])
		if err != nil {
			err = errors.New("TOPIC ID argument must be a number")
			printError(err)
		}

		// Get abstract id argument
		abstractID, err := strconv.Atoi(args[1])
		if err != nil {
			err = errors.New("ABSTRACT ID argument must be a number")
			printError(err)
		}

		// Get topic
		topics, err := database.GetTopics()
		if err != nil {
			err = errors.New("Getting topics: " + err.Error())
			printError(err)
		}

		topic := topics[topicID-1]

		// Remove abstract
		if err = topic.RemoveAbstract(abstractID - 1); err != nil {
			err = errors.New("Removing abstract: " + err.Error())
			printError(err)
		}
	},
}

func init() {
	RootCmd.AddCommand(raCmd)
}
