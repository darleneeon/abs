package cmd

import (
	"errors"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/darleneeon/abs/database"
)

const rtUsage = "rt [TOPIC ID]"

// rtCmd represents the rt command
var rtCmd = &cobra.Command{
	Use:   rtUsage,
	Short: "Remove topic",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {

		// Get topic id argument
		id, err := strconv.Atoi(args[0])
		if err != nil {
			err = errors.New("ID argument must be a number")
			printError(err)
		}

		// Get topics
		topics, err := database.GetTopics()
		if err != nil {
			err = errors.New("Getting topics: " + err.Error())
			printError(err)
		}

		// Check if id is available
		if id > len(topics) {
			err = errors.New("Topic with given ID is not found")
			printError(err)
		}

		// Remove topic
		if err = topics[id].Remove(); err != nil {
			err = errors.New("Removing topic: " + err.Error())
			printError(err)
		}
	},
}

func init() {
	RootCmd.AddCommand(rtCmd)
}
