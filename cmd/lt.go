package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/darleneeon/abs/database"
)

// tlCmd represents the tl command
var ltCmd = &cobra.Command{
	Use:   "lt",
	Short: "Show list of the topics",
	Run: func(cmd *cobra.Command, args []string) {

		// Get topics
		topics, err := database.GetTopics()
		if err != nil {
			err = errors.New("Getting topics: " + err.Error())
			printError(err)
		}

		for i := 1; i < len(topics)+1; i++ {
			fmt.Printf("[%d]\t%s\n", i, topics[i-1].Heading)
		}
	},
}

func init() {
	RootCmd.AddCommand(ltCmd)

}
