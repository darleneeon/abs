package cmd

import (
	"errors"

	"github.com/spf13/cobra"
	"gitlab.com/darleneeon/abs/database"
)

const ctUsage = "ct [TOPIC HEADING]"

// ctCmd represents the ct command
var ctCmd = &cobra.Command{
	Use:   ctUsage,
	Short: "Create new topic",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {

		// Create topic instance
		topic := database.Topic{Heading: args[0]}

		// Save topic in the database
		if err := topic.Save(); err != nil {
			err = errors.New("Saving topic: " + err.Error())
			printError(err)
		}
	},
}

func init() {
	RootCmd.AddCommand(ctCmd)
}
