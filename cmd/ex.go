package cmd

import (
	"encoding/json"
	"errors"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/darleneeon/abs/database"
)

// backupCmd represents the backup command
var exCmd = &cobra.Command{
	Use:   "ex",
	Short: "Export existing data to json file",
	Run: func(cmd *cobra.Command, args []string) {

		// Get all topics
		topics, err := database.GetTopics()
		if err != nil {
			err = errors.New("Getting topics: " + err.Error())
			printError(err)
		}

		// Create a json file
		file, err := os.Create("abs_export.json")
		if err != nil {
			err = errors.New("Creating file: " + err.Error())
			printError(err)
		}
		defer file.Close()

		// Write data to the file
		marhalled, err := json.Marshal(&topics)
		if err != nil {
			printError(err)
		}

		if _, err = file.Write(marhalled); err != nil {
			printError(err)
		}
	},
}

func init() {
	RootCmd.AddCommand(exCmd)
}
