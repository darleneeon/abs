package cmd

import (
	"errors"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/darleneeon/abs/database"
)

const laUsage = "la [TOPIC ID]"

// alCmd represents the al command
var laCmd = &cobra.Command{
	Use:   laUsage,
	Args:  cobra.MinimumNArgs(1),
	Short: "Show list of abstracts for the topic",
	Run: func(cmd *cobra.Command, args []string) {

		// Get topic id argument
		id, err := strconv.Atoi(args[0])
		if err != nil {
			err = errors.New("TOPIC ID argument must be a number!")
			printError(err)
		}

		// Get topic
		topic, err := database.GetTopic(id)
		if err != nil {
			err = errors.New("Getting topic: " + err.Error())
			printError(err)
		}

		// Print abstracts
		for i := 1; i < len(topic.Abstracts)+1; i++ {
			abstract := topic.Abstracts[i-1]
			fmt.Printf("[%d]\t%s %s\n", i, abstract.Heading, abstract.Link)
		}
	},
}

func init() {
	RootCmd.AddCommand(laCmd)
}
