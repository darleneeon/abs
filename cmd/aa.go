package cmd

import (
	"errors"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/darleneeon/abs/database"
)

const aaUsage = "aa [TOPIC ID] [HEADING] [WEB LINK]"

// aaCmd represents the aa command
var aaCmd = &cobra.Command{
	Use:   aaUsage,
	Short: "Add new abstract to the topic",
	Args:  cobra.MinimumNArgs(3),
	Run: func(cmd *cobra.Command, args []string) {

		// Get topic id argument
		id, err := strconv.Atoi(args[0])
		if err != nil {
			err = errors.New("TOPIC ID argument must be a number!")
			printError(err)
		}

		// Get heading argument
		heading := args[1]

		// Get web link argument
		webLink := args[2]

		// Create abstract
		abstract := database.Abstract{Heading: heading, Link: webLink}

		// Save abstract
		topic, err := database.GetTopic(id)
		if err != nil {
			err = errors.New("Getting topic: " + err.Error())
			printError(err)
		}

		if err = topic.AddAbstract(abstract); err != nil {
			err = errors.New("Adding abstract: " + err.Error())
			printError(err)
		}
	},
}

func init() {
	RootCmd.AddCommand(aaCmd)
}
